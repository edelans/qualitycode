---
title: about
layout: page
---

Site created with hexo using a modified hueman theme and hosted for free on Gitlab Pages with a free SSL certificate from Let's Encrypt!

I'm passionate about leveraging on computers to do more and better, and I wanted to summarize in one place all I learned in my 2 years as a startup CTO experiment.
