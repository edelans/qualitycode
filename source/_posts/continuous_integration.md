---
title: Continuous integration
tags: automation
categories: automation
---

# Continuous integration

Continuous integration is the automation of deployment.

You can do it in a lot of different ways. The simplest form would be something like this : you push to master, it is deployed on your prod server right away.

A more complicated form would take into account tasks like build, tests validation, multiple branch linked to different environments (dev > staging, master > production).
