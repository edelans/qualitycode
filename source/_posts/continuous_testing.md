---
title: Continuous testing
tags: automation
categories: automation
---
# Continuous testing

Continuous testing is the automation of testing after each commit.

This is really powerful because it means that you will be able to fix the breaking change as soon as you introduce it ! (Assumig you keep your commits small, which you should really do).

The problem is that you need to have an excellent testing coverage of your code if you want it to be perfect. Nonetheless, even if you have a limited coverage (say you only test the most critical parts of your code), continuous testing will still prove very effective to mitigate the risks of breaking things badly.

My advice would be to start covering only critical parts of the code, and grow the test suite as you go (=as you add critical code / as you break things). Keeping it small at the beginning will help a lot (relieve yourself from the burden of the 100% coverage objective !).

It's like an insurance : you have to pay for it (write tests), and the more you pay (= the more you write tests), the more you will be covered. Still, there are things that cannot be tested very well. But continuous testing will nonetheless mitigate very effectively a lot of risks, and will buy you a peace of mind I cannot emphasize enough !
