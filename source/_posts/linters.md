---
title: Linters
tags: automation
categories: automation
---
Linters are ...

They are like a robot pear reviewing your code in real time:
- the are failureproof regarding clearly defined rules they are set up for (yet this rules are simple)
- they will allow no exception (even when it can be discussed)

Linters are great to avoid basic mistakes.
Forgot a ```;``` at the end of your javascript line ? declared a var but didn't use it ? typo in your var that make it looks like you try to use a var you didn't declare ? switching between camelCase and the_other_case ? It will save you a lot of time.

Linters are a great way to improve your programming skills.
You will have intant feedback that will enable you to improve your code as you type it. I remember an interview when I had to complete a ruby exercise ahead of the meeting. The problem was I had no experience with ruby. They told me it was OK, they just wanted to read the logic, and I could do it in another language if I wanted to, but ruby was undoubtedly a big plus. The first thing I did was to install the rubocop plugin in my text editor (I use atom and it's just one click away from the inegrated package manager). it saved me a lot of time and I could write ruby much more efficiently than going from one syntax error to another.

Linters are great to enforce consistency in your code.
If you collaborate on code, (what I hope you do because it's another great way to make huge progress in no time -TODO: put a link to the other article ), you know that reading someone else's code can be really painfull if you do not share the same conventions. Linters can help you enforce automatically and virually painlessly enforce the application of those shared standards. You agree on a convention (sometimes linters can be tuned at rule level) and you collectively decide that no code should hit the master branch if the linter have any complain about it (you can also use pre-commit hooks (TODO: link here) to automatically enforce this ! This kind of trick will lower the collaboration difficulties in a very effective way.

How to do it :
- on your CLI
- with a plugin of your editor
