---
title: Continuous learning
tags: spirit
categories: spirit
---

# Continuous learning

BENEFITS:
- CEO / CFO conversation
- motivation to keep excellent people

'Train people well enough so they can eave. Treat them well enough so they don't want to !' --Sir Richard Branson

-> There is no alternative : if you want top notch performers, and you want them to stay, you want your employees to keep on learning and experimenting new things.

Sounds obvious right ? It's easy to say, every companies out there will tell you they are in favor of continuous learning. But the truth is : it's hard to apply in your day to day operations, and the same companies seldom behave as they say they do...TODO: mettre en pratique. Amazongly enough there are some easy tricks that would do it !

## Programming contests
I met a company where the dev team (approx 10 people) would stop working on the business code on friday afternoon to do a programming contest. The CTO would choose a kata (see hackerrank or codewars TODO: links), everybody would work on it, and then the CTO organise a debriefing session where he compared and discussed with his team the different approachs. The upsides where awesome : mutual enrichment, sharing of coding style and good practice, very strong progress from novice devs, nice competition between the team, great team building experience... The only thing is the rythm is hard to keep and motivation started to slow down after a couple of months. They adapted the frequency from once a week to once every two weeks.

## Talks
Most of tech companies have employees passionate about something (a hobby, a side project, a technology / tool they used in another context...). It's a great experience to provide those people the opportunity to share what they know. For instance if a new employee enjoyed a past experience and would like to import it in its new team, if it make sense it is paramount to enable him to do so ! It will build trust among the team, grow everyone and provide opportunity to grow, discuss and share opinions.

If you do not have inside expertise available you can also find external expertise. Brown Bag Lunch is something widespread in the US and it's also getting steam abroad. The website (TODO: link) references experts available in your city to give a talk in exchange of a lunch. You will find there experts on plenty different topics.


## Exernal gurus
Sometimes when you are not familiar with a tool / new technology, you can waste an awful lot of time. In those cases, reaching out to an expert of that field is priceless. For important missions, you can find consultants (find them on linkedin / github / meetups of the community you are targeting). For more modest interventions, you can find experts on codementors that will solve your problem in no time ! A company I met provided its devs a 100$/month credit on codementor that they could use on whatever they wanted (even if it was not directly related to their work). I find this pretty awesome.

When looking for external expertise, one thing is sur: ask for the bests. "Buying quality is choosing to cry only one time !"

+ image : there is always someone who will do it cheaper
