---
title: Atomic commits
tags: process
categories: process
---
# Keep your commits small

## Why ?
- Because your teammates will thank you for that (it is much easier to peer review and to understand what the other dev did)
- Because you can find buggy commits more easily (see ```git bisect``` if are interested in bringing your bug hunting to another level)
- Because it assure you you won't lose your work
- Finally, because it is so cheap that the ROI is skyrocketing.



## What is a good commit message ?

see https://chris.beams.io/posts/git-commit/



## Use shortcuts to standardize your workflow
