---
title: Deployment oneliner
tags: process
categories: process
---

# Deployment oneliner

Production is life.

If you keep working on code that will not hit your production server, **you are wasting your time**.

If you think your changes are not ready for production, there are some reasons to think you haven't broken down your work on reasonably sized chunks, and that your code will never hit the production server. And if it does, the changes will be so big that the chances something breaks are huge compared to smaller chunks. (TODO: link to small commits) (TODO: add Number of deploy in a day for agile teams : github, amazon, ... )

I have met some organizations where deployment was done once every two months and it was a dreaded time because every thing was done by hand and at virtually every step the dev pushing the code on production feared that something could go wrong.

If you fear something, it's because you don't do it often enough. You have to make it ultra simple to push code into production ! You must have a way to deploy your code in just one line. Use a script, or use the tool that does it for you, but do it !


To go further :
- use a staging environment
- jump to continuous integration
