---
title: Automation
tags: automation
categories: automation
---

The human mind is incredibly powerful in doing complex tasks that computers will never be able to do in the forseeable future. On the other hand, the same human mind can prove himself really shitty with small dumb disgraceful tasks. And this is exactly where computer thrive ! Checking the syntax of your code, run your tests on each commits, deploy your webapp, check for security vulnerabilities, check for dependencies update... These tasks can all be automated and this is where you can truly leverage on computers to make you ship products more efficiently. When you use the right tools to assist you, you can have the best of the 2 worlds !
