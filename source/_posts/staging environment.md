---
title: Staging environment
tags: process
categories: process
---
# Use a staging environment

A staging environment is an environment similar to your production environment where you deploy your "work in progress" branch (most of the time the 'dev' branch).

The more iso-prod, the more you will be comfortable to push the changes to your production environment !

Same dependecies versions, same softare installed, same configuration (firewall...). And something very useful : same data ! I recommend setting up a script that regularly export your database from the prod to the staging environment. This way you will be able to experiment the impact of you changes on the production data without any risk of corrupting the data of your users.
